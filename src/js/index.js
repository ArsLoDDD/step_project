rndNum = () => {
	const bigNum = Math.floor(Math.random() * 900) + 100
	const frstNum = Math.floor(Math.random() * 9) + 1
	return String(frstNum) + ',' + String(bigNum)
}
$('.editor__btn-counter').each(function () {
	$(this).text(rndNum())
})
$('.header__burger').click(function (e) {
	$('.header__burger-menu').toggleClass('active')
	if ($('.header__burger-menu').hasClass('active')) {
		$('.header__burger').css({
			transform: 'rotate(90deg)',
			transition: 'transform 0.5s',
		})
		setTimeout(() => {
			$('.header__burger').css({
				backgroundImage: "url('./../img/close.svg')",
			})
		}, 50)
	} else {
		$('.header__burger').css({
			transform: 'rotate(-180deg)',
			transition: 'transform 0.5s',
		})
		setTimeout(() => {
			$('.header__burger').css({
				backgroundImage: "url('./../img/burger.svg')",
			})
		}, 50)
	}
})
