# Forkio Project

**Step Project**

## Список Використаних технологій:

- gulp
- gulp-htmlmin
- gulp-concat
- gulp-terser
- gulp-clean-css
- gulp-clean
- browser-sync
- gulp-sass
- gulp-autoprefixer
- gulp-imagemin
- gulp-cache

### Учасники:

- **Shkolnikov Arseniy** - _Виконував завдання #2_
- **Kandyba Vyacheslav** - _Виконував завдання #1_

[Repository](https://gitlab.com/ArsLoDDD/step_project)
[Deploy](step-project.vercel.app)
