import gulp from 'gulp'
import htmlmin from 'gulp-htmlmin'
import concat from 'gulp-concat'
import terser from 'gulp-terser'
import cleanCSS from 'gulp-clean-css'
import clean from 'gulp-clean'
import browserSync from 'browser-sync'
import dartSass from 'sass'
import gulpSass from 'gulp-sass'
import autoprefixer from 'gulp-autoprefixer'
import imagemin from 'gulp-imagemin'
import cache from 'gulp-cache'

const sass = gulpSass(dartSass)
const { src, dest, watch, series, task, parallel } = gulp

browserSync.create()

const dev = () => {
	browserSync.init({
		logPrefix: '',
		server: {
			baseDir: './dist',
		},
	})
	watch(
		'./src/**/*',
		series(cleanDist, parallel(html, scss, js, image), next => {
			browserSync.reload()
			next()
		})
	)
}

const scss = () => {
	return src('./src/styles/**/*.scss')
		.pipe(sass().on('error', sass.logError))
		.pipe(concat('styles.min.css'))
		.pipe(cleanCSS())
		.pipe(
			autoprefixer({
				cascade: false,
			})
		)
		.pipe(dest('./dist/styles'))
}

const image = () => {
	return gulp
		.src('./src/img/**/*')
		.pipe(cache(imagemin()))
		.pipe(gulp.dest('./dist/img'))
}

const cleanDist = () => {
	return src('./dist', { read: false }).pipe(clean())
}

const html = () => {
	return src('./src/*.html')
		.pipe(htmlmin({ collapseWhitespace: true }))
		.pipe(dest('./dist'))
}
const js = () => {
	return src('./src/js/**/*.js')
		.pipe(concat('scripts.min.js'))
		.pipe(terser())
		.pipe(dest('./dist/js'))
}
// const scss = () => {
// 	return src('./src/styles/**/*.scss')
// 		.pipe(concat('main.scss'))
// 		.pipe(cleanCSS())
// 		.pipe(dest('./dist/styles'))
// }
task('build', parallel(html, scss, js, image))
task('dev', series('build', dev))
